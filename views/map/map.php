<?php
use yii\helpers\Url;
?>
<h2>Map</h2>
<div class="row">
    <div class="col-md-8">
        <table>
            <?php foreach($renderMap as $collumn): ?>
                <tr>
                    <?php foreach($collumn as $row): ?>
                        <?php if($row == '.'): ?>
                            <td style="background-color: #00CC00 ">
                                <?= $row ?>
                            </td>
                        <?php elseif($row == 'x'): ?>
                            <td style="background-color: #f5e79e">
                                <?= $row ?>
                            </td>
                        <?php elseif(is_numeric($row)): ?>
                            <td style="background-color: orangered">
                                <?= $row ?>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-6">
                <form action="<?= Url::to('/map/step') ?>">
                    <input type="hidden" name="step" value="left">
                    <input type="hidden" name="id" value="<?= $players[0]['id'] ?>">
                    <input type="submit" class="btn btn-success" value="left">
                </form>
            </div>
            <div class="col-md-6">
                <form action="<?= Url::to('/map/step') ?>">
                    <input type="hidden" name="step" value="right">
                    <input type="hidden" name="id" value="<?= $players[0]['id'] ?>">
                    <input type="submit" class="btn btn-success" value="right">
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <form action="<?= Url::to('/map/step') ?>">
                    <input type="hidden" name="step" value="up">
                    <input type="hidden" name="id" value="<?= $players[0]['id'] ?>">
                    <input type="submit" class="btn btn-success" value="up">
                </form>
            </div>
            <div class="col-md-6">
                <form action="<?= Url::to('/map/step') ?>">
                    <input type="hidden" name="step" value="down">
                    <input type="hidden" name="id" value="<?= $players[0]['id'] ?>">
                    <input type="submit" class="btn btn-success" value="down">
                </form>
            </div>
        </div>

    </div>
</div>

