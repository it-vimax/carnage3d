<?php
use yii\helpers\Html;
?>
<ul class="chat">
    <?php
    foreach($messages as $message): ?>
        <li>
            <b><?= $message->author ?></b>
            <p><?= $message->text ?></p>
        </li>
    <?php endforeach; ?>
</ul>
<hr>
<form action="/chat/message" method="get">
    <input type="text" name="author">
    <br>
    <br>
    <textarea name="message" id="" cols="30" rows="10" style="width:100%;height:50px"></textarea>
    <input type="submit" value="отправить">
</form>
