<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 28.12.2017
 * Time: 22:50
 */

namespace app\controllers;

use app\models\Messages;
use app\socketEvent\DragBox;
use app\socketEvent\NewMessageAdded;
use app\socketEvent\NewPositionEvent;
use Yii;
use yii\web\Controller;

class ChatController extends Controller
{
    public function actionIndex()
    {
        $messages = Messages::find()->all();
        return $this->render('index', compact('messages'));
    }

    public function actionThreejs()
    {
        return $this->render('threejs');
    }

    public function actionPosition()
    {
        $newPosition = Yii::$app->request->get('position');
        (new NewPositionEvent(['position' => $newPosition]))->toOthers()->broadcast();
    }
    
    public function actionMessage()
    {
        $getAuthor = Yii::$app->request->get('author');
        $getMessage = Yii::$app->request->get('message');
        $message = new Messages();
        $message->text = $getMessage;
        $message->author = $getAuthor;
        $message->save();
        (new NewMessageAdded([
            'author' => $message->author,
            'message' => $message->text,
        ]))->toOthers()->broadcast();

        return $this->redirect('/chat/index');
    }

//    public function actionDragBox()
//    {
//        (new DragBox([
//            'pageX' => Yii::$app->request->get('pageX'),
//            'pageY' => Yii::$app->request->get('pageY')
//        ]))->toOthers()->broadcast(); // срабатывает событие
//        return Yii::$app->response->setStatusCode(200);
//    }

}