<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 18.01.2018
 * Time: 19:08
 */

namespace app\controllers;


use app\models\Players;
use yii\web\Controller;

class SeedController extends Controller
{
    public function actionPlayers()
    {
        $players = [
            [
                'name' => 'Maks',
                'position_x' => 8,
                'position_y' => 10,
            ],
            [
                'name' => 'Viko',
                'position_x' => 13,
                'position_y' => 15,
            ],
        ];
        
        foreach($players as $player)
        {
            $playerModel = new Players();
            $playerModel->name = $player['name'];
            $playerModel->position_x = $player['position_x'];
            $playerModel->position_y = $player['position_y'];
            $playerModel->save();
        }
        return $this->asJson($players);
    }
}