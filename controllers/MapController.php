<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 16.01.2018
 * Time: 22:51
 */

namespace app\controllers;


use app\libs\Map;
use app\models\Players;
use Yii;
use yii\web\Controller;

class MapController extends Controller
{
    public function actionCollision()
    {
        $itemRender = [];

        $players = Players::find()->asArray()->all();
        $itemRender['players'] = $players;

        $playerBorder = $this->playerBorder($players);
        $itemRender['playersBorder'] = $playerBorder;
        $map = new Map();
        $renderMap = $map->renderMap(20, 20, $itemRender);
        return $this->render('map', compact('renderMap', 'players'));
    }

    public function actionStep()
    {
        $go = 1;
        $playerId = Yii::$app->request->get('id');
        $step = Yii::$app->request->get('step');
        $player = Players::find()->where(['id' => $playerId])->all()[0];
        switch($step)
        {
            case 'left':
                $player->position_y -= $go;
                break;
            case 'right':
                $player->position_y += $go;
                break;
            case 'up':
                $player->position_x -= $go;
                break;
            case 'down':
                $player->position_x += $go;
                break;
        }
        $player->save();
        return $this->redirect('/map/collision');
    }

    public function playerBorder($playersArr)
    {
        $res = [];
        foreach($playersArr as  $player)
        {
            $step[$player['id']]['border_x'][] = (integer)$player['position_x'] - 1;
            $step[$player['id']]['border_x'][] = (integer)$player['position_x'];
            $step[$player['id']]['border_x'][] = (integer)$player['position_x'] + 1;
            $step[$player['id']]['border_y'][] = (integer)$player['position_y'] - 1;
            $step[$player['id']]['border_y'][] = (integer)$player['position_y'];
            $step[$player['id']]['border_y'][] = (integer)$player['position_y'] + 1;
            foreach($step[$player['id']]['border_x'] as $item)
            {
                $res[$player['id']][$item] = $step[$player['id']]['border_y'];
            }
        }
        return $res;
    }

    public function actionGetStep()
    {
        $thisX = (int)Yii::$app->request->get('thisX');
        $thisY = (int)Yii::$app->request->get('thisY');
        $stepX = (int)Yii::$app->request->get('stepX');
        $stepY = (int)Yii::$app->request->get('stepY');

        // куда может ходить
        $positionX[] = $thisX - 1;
        $positionX[] = $thisX;
        $positionX[] = $thisX + 1;

        $positionY[] = $thisY - 1;
        $positionY[] = $thisY;
        $positionY[] = $thisY + 1;

        $res = [];
        foreach($positionX as $item)
        {
            $res[$item] = $positionY;
        }
        if(is_array( $res[$stepX]))
        {
            $go = in_array($stepY, $res[$stepX]);
        }
        else
        {
            $go = false;
        }
        return $this->asJson([[$thisX, $thisY, $stepX, $stepY], $res, $go]);
    }
}