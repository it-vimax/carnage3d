<?php
namespace app\socketEvent;
use mkiselev\broadcasting\channels\PrivateChannel;
use mkiselev\broadcasting\events\BroadcastEvent;
class DragBox extends BroadcastEvent
{
    public $pageX;
    public $pageY;

    public function __constructor($pageX, $pageY)
    {
        $this->pageX = $pageX;
        $this->pageY = $pageY;
    }

    /**
     * Get the channels the event should broadcast on
     *
     * @return string|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('box');
    }

    public function broadcastAs()
    {
        return 'drag';
    }
}