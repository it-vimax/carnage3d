"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Server = require("./libs/Server");
var Server = _Server.Server;
var Main = (function () {
    function Main() {
    }
    Main.prototype.main = function () {
        var server = new Server(6001);
        server.start();
        server.psubscribe();
        server.pmessage();
    };
    return Main;
}());
new Main().main();
//# sourceMappingURL=main.js.map