"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Lights = require("./Lights");
var Lights = _Lights.Lights;
var _Sky = require("./Sky");
var Sky = _Sky.Sky;
var _Field = require("./Field");
var Field = _Field.Field;
var Scene1 = (function () {
    function Scene1() {
    }
    Scene1.init = function () {
        Scene1.HEIGHT = window.innerHeight;
        Scene1.WIDTH = window.innerWidth;
        Scene1.aspectRatio = Scene1.WIDTH / Scene1.HEIGHT;
        Scene1.fieldOfView = 60;
        Scene1.nearPlane = 1;
        Scene1.farPlane = 10000;
        Scene1.lights = new Lights();
        Scene1.createScene();
        Scene1.createLights();
        Scene1.createSky();
        Scene1.createField();
        Scene1.loop();
    };
    Scene1.loop = function () {
        Scene1.sky.mesh.rotation.z += 0.01;
        Scene1.field.mesh.rotation.y += 0.01;
        Scene1.renderer.render(Scene1.scene, Scene1.camera);
        requestAnimationFrame(Scene1.loop);
    };
    Scene1.createScene = function () {
        Scene1.scene = new THREE.Scene();
        Scene1.camera = new THREE.PerspectiveCamera(Scene1.fieldOfView, Scene1.aspectRatio, Scene1.nearPlane, Scene1.farPlane);
        Scene1.scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);
        Scene1.camera.position.x = 0;
        Scene1.camera.position.z = 200;
        Scene1.camera.position.y = 100;
        Scene1.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
        Scene1.renderer.setSize(Scene1.WIDTH, Scene1.HEIGHT);
        Scene1.renderer.shadowMap.enabled = true;
        Scene1.container = document.getElementById('world');
        Scene1.container.appendChild(Scene1.renderer.domElement);
        window.addEventListener('resize', Scene1.handleWindowResize, false);
    };
    Scene1.handleWindowResize = function () {
        Scene1.HEIGHT = window.innerHeight;
        Scene1.WIDTH = window.innerWidth;
        Scene1.renderer.setSize(this.WIDTH, Scene1.HEIGHT);
        Scene1.camera.aspect = this.WIDTH / Scene1.HEIGHT;
        Scene1.camera.updateProjectionMatrix();
    };
    Scene1.createLights = function () {
        Scene1.lights.init();
        Scene1.scene.add(Scene1.lights.hemisphereLight);
        Scene1.scene.add(Scene1.lights.shadowLight);
    };
    Scene1.createSky = function () {
        Scene1.sky = new Sky();
        Scene1.sky.mesh.position.y = -600;
        Scene1.scene.add(Scene1.sky.mesh);
    };
    Scene1.createField = function () {
        Scene1.field = new Field();
        Scene1.field.mesh.position.y = 100;
        Scene1.scene.add(Scene1.field.mesh);
    };
    return Scene1;
}());
exports.Scene1 = Scene1;
//# sourceMappingURL=Scene1.js.map