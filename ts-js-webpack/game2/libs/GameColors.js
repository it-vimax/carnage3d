"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameColors = (function () {
    function GameColors() {
    }
    return GameColors;
}());
GameColors.red = 0xf25346;
GameColors.white = 0xd8d0d1;
GameColors.brown = 0x59332e;
GameColors.pink = 0xF5986E;
GameColors.brownDark = 0x23190f;
GameColors.blue = 0x68c3c0;
exports.GameColors = GameColors;
//# sourceMappingURL=GameColors.js.map