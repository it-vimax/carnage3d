<?php

/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 18.01.2018
 * Time: 19:26
 */
namespace app\libs;

class Map
{
    public function renderMap($countX, $countY, $items)
    {
        $res = [];
        for($x = 0; $x < $countX; $x++)
        {
            for($y = 0; $y < $countY; $y++)
            {
                // рисуем пустые поля
                $res[$x][$y] = '.';
                // отрисовка ограничения хода игрока
                foreach($items['playersBorder'] as $border)
                {
                    if(isset($border[$x]) && in_array($y, $border[$x]))
                    {
                        $res[$x][$y] = 'x';
                    }
                }
                // отрисовка игроков
                foreach($items['players'] as $player)
                {
                    if($x == $player['position_x'] && $y == $player['position_y'])
                    {
                        $res[$x][$y] = $player['id'];
                        break;
                    }
                }
            }
        }
        return $res;
    }
}