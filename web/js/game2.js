/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _Scene1 = __webpack_require__(1);
var Scene1 = _Scene1.Scene1;
window.addEventListener('load', Scene1.init, false);
//# sourceMappingURL=main.js.map

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _Lights = __webpack_require__(2);
var Lights = _Lights.Lights;
var _Sky = __webpack_require__(3);
var Sky = _Sky.Sky;
var _Field = __webpack_require__(7);
var Field = _Field.Field;
var Scene1 = (function () {
    function Scene1() {
    }
    Scene1.init = function () {
        Scene1.HEIGHT = window.innerHeight;
        Scene1.WIDTH = window.innerWidth;
        Scene1.aspectRatio = Scene1.WIDTH / Scene1.HEIGHT;
        Scene1.fieldOfView = 60;
        Scene1.nearPlane = 1;
        Scene1.farPlane = 10000;
        Scene1.lights = new Lights();
        Scene1.createScene();
        Scene1.createLights();
        Scene1.createSky();
        Scene1.createField();
        Scene1.loop();
    };
    Scene1.loop = function () {
        Scene1.sky.mesh.rotation.z += 0.01;
        Scene1.field.mesh.rotation.y += 0.01;
        Scene1.renderer.render(Scene1.scene, Scene1.camera);
        requestAnimationFrame(Scene1.loop);
    };
    Scene1.createScene = function () {
        Scene1.scene = new THREE.Scene();
        Scene1.camera = new THREE.PerspectiveCamera(Scene1.fieldOfView, Scene1.aspectRatio, Scene1.nearPlane, Scene1.farPlane);
        Scene1.scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);
        Scene1.camera.position.x = 0;
        Scene1.camera.position.z = 200;
        Scene1.camera.position.y = 100;
        Scene1.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
        Scene1.renderer.setSize(Scene1.WIDTH, Scene1.HEIGHT);
        Scene1.renderer.shadowMap.enabled = true;
        Scene1.container = document.getElementById('world');
        Scene1.container.appendChild(Scene1.renderer.domElement);
        window.addEventListener('resize', Scene1.handleWindowResize, false);
    };
    Scene1.handleWindowResize = function () {
        Scene1.HEIGHT = window.innerHeight;
        Scene1.WIDTH = window.innerWidth;
        Scene1.renderer.setSize(this.WIDTH, Scene1.HEIGHT);
        Scene1.camera.aspect = this.WIDTH / Scene1.HEIGHT;
        Scene1.camera.updateProjectionMatrix();
    };
    Scene1.createLights = function () {
        Scene1.lights.init();
        Scene1.scene.add(Scene1.lights.hemisphereLight);
        Scene1.scene.add(Scene1.lights.shadowLight);
    };
    Scene1.createSky = function () {
        Scene1.sky = new Sky();
        Scene1.sky.mesh.position.y = -600;
        Scene1.scene.add(Scene1.sky.mesh);
    };
    Scene1.createField = function () {
        Scene1.field = new Field();
        Scene1.field.mesh.position.y = 100;
        Scene1.scene.add(Scene1.field.mesh);
    };
    return Scene1;
}());
exports.Scene1 = Scene1;
//# sourceMappingURL=Scene1.js.map

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Lights = (function () {
    function Lights() {
    }
    Lights.prototype.init = function () {
        this.hemisphereLight = new THREE.HemisphereLight(0xaaaaaa, 0x000000, .9);
        this.shadowLight = new THREE.DirectionalLight(0xffffff, .9);
        this.shadowLight.position.set(150, 350, 350);
        this.shadowLight.castShadow = true;
        this.shadowLight.shadow.camera.left = -400;
        this.shadowLight.shadow.camera.right = 400;
        this.shadowLight.shadow.camera.top = 400;
        this.shadowLight.shadow.camera.bottom = -400;
        this.shadowLight.shadow.camera.near = 1;
        this.shadowLight.shadow.camera.far = 1000;
        this.shadowLight.shadow.mapSize.width = 2048;
        this.shadowLight.shadow.mapSize.height = 2048;
    };
    return Lights;
}());
exports.Lights = Lights;
//# sourceMappingURL=Lights.js.map

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _Cloud = __webpack_require__(4);
var Cloud = _Cloud.Cloud;
var Sky = (function () {
    function Sky() {
        this.mesh = new THREE.Object3D();
        this.nClouds = 20;
        this.clouds = [];
        var stepAngle = Math.PI * 2 / this.nClouds;
        for (var i = 0; i < this.nClouds; i++) {
            var c = new Cloud();
            this.clouds.push(c);
            var a = stepAngle * i;
            var h = 750 + Math.random() * 200;
            c.mesh.position.y = Math.sin(a) * h;
            c.mesh.position.x = Math.cos(a) * h;
            c.mesh.position.z = -400 - Math.random() * 400;
            c.mesh.rotation.z = a + Math.PI / 2;
            var s = 1 + Math.random() * 2;
            c.mesh.scale.set(s, s, s);
            this.mesh.add(c.mesh);
        }
    }
    return Sky;
}());
exports.Sky = Sky;
//# sourceMappingURL=Sky.js.map

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _GameColors = __webpack_require__(6);
var GameColors = _GameColors.GameColors;
var Cloud = (function () {
    function Cloud() {
        this.mesh = new THREE.Object3D();
        this.mesh.name = "cloud";
        var geom = new THREE.BoxGeometry(20, 20, 20);
        var mat = new THREE.MeshPhongMaterial({
            color: GameColors.white,
        });
        var nBlocs = 3 + Math.floor(Math.random() * 3);
        for (var i = 0; i < nBlocs; i++) {
            var m = new THREE.Mesh(geom.clone(), mat);
            m.position.x = i * 15;
            m.position.y = Math.random() * 10;
            m.position.z = Math.random() * 10;
            m.rotation.z = Math.random() * Math.PI * 2;
            m.rotation.y = Math.random() * Math.PI * 2;
            var s = 0.1 + Math.random() * 0.9;
            m.scale.set(s, s, s);
            m.castShadow = true;
            m.receiveShadow = true;
            this.mesh.add(m);
        }
    }
    return Cloud;
}());
exports.Cloud = Cloud;
//# sourceMappingURL=Cloud.js.map

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var GameColors = (function () {
    function GameColors() {
    }
    return GameColors;
}());
GameColors.red = 0xf25346;
GameColors.white = 0xd8d0d1;
GameColors.brown = 0x59332e;
GameColors.pink = 0xF5986E;
GameColors.brownDark = 0x23190f;
GameColors.blue = 0x68c3c0;
exports.GameColors = GameColors;
//# sourceMappingURL=GameColors.js.map

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var _GameColors = __webpack_require__(6);
var GameColors = _GameColors.GameColors;
var Field = (function () {
    function Field() {
        this.mesh = new THREE.Object3D();
        this.mesh.name = "Field";
        var geom = new THREE.BoxGeometry(100, 100, 10);
        var mat = new THREE.MeshPhongMaterial({
            color: GameColors.blue
        });
        var m = new THREE.Mesh(geom, mat);
        m.position.x = 100;
        m.position.y = 10;
        m.position.z = 10;
        m.castShadow = true;
        m.receiveShadow = true;
        this.mesh.add(m);
        var geom2 = new THREE.BoxGeometry(100, 10, 10);
        var mat2 = new THREE.MeshPhongMaterial({
            color: GameColors.red
        });
        var m2 = new THREE.Mesh(geom2, mat2);
        m2.position.x = 0;
        m2.position.y = 0;
        m2.position.z = 0;
        m2.castShadow = true;
        m2.receiveShadow = true;
        this.mesh.add(m2);
    }
    return Field;
}());
exports.Field = Field;
//# sourceMappingURL=Field.js.map

/***/ })
/******/ ]);