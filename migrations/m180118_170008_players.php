<?php

use yii\db\Migration;

/**
 * Class m180118_170008_players
 */
class m180118_170008_players extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('players', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'position_x' => $this->integer(),
            'position_y' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180118_170008_players cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180118_170008_players cannot be reverted.\n";

        return false;
    }
    */
}
