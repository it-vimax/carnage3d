import _Lights = require('./Lights'); import Lights = _Lights.Lights;
import _Sky = require('./Sky'); import Sky = _Sky.Sky;
import _Field = require('./Field'); import Field = _Field.Field;

export class Scene1
{
    public static HEIGHT:number;
    public static WIDTH:number;
    public static scene:any;
    public static camera:any;
    public static container:any;
    public static renderer:any;

    private static fieldOfView:number;
    private static aspectRatio:number;
    private static nearPlane:number;
    private static farPlane:number;
    private static lights:Lights;
    private static sky:Sky;

    private static field:Field;

    public static init():void
    {
        Scene1.HEIGHT = window.innerHeight;
        Scene1.WIDTH = window.innerWidth;
        Scene1.aspectRatio = Scene1.WIDTH / Scene1.HEIGHT;
        Scene1.fieldOfView = 60;
        Scene1.nearPlane = 1;
        Scene1.farPlane = 10000;
        Scene1.lights = new Lights();

        Scene1.createScene();
        Scene1.createLights();
        Scene1.createSky();
        Scene1.createField();
        Scene1.loop();
    }

    public static loop():void
    {
        Scene1.sky.mesh.rotation.z += 0.01;
        Scene1.field.mesh.rotation.y += 0.01;
        Scene1.renderer.render(Scene1.scene, Scene1.camera);
        requestAnimationFrame(Scene1.loop);
    }

    private static createScene():void
    {
        Scene1.scene = new THREE.Scene();
        Scene1.camera = new THREE.PerspectiveCamera(
            Scene1.fieldOfView,
            Scene1.aspectRatio,
            Scene1.nearPlane,
            Scene1.farPlane
        );
        Scene1.scene.fog = new THREE.Fog(0xf7d9aa, 100,950);
        Scene1.camera.position.x = 0;
        Scene1.camera.position.z = 200;
        Scene1.camera.position.y = 100;

        Scene1.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
        Scene1.renderer.setSize(Scene1.WIDTH, Scene1.HEIGHT);
        Scene1.renderer.shadowMap.enabled = true;
        Scene1.container = document.getElementById('world');
        Scene1.container.appendChild(Scene1.renderer.domElement);
        window.addEventListener('resize', Scene1.handleWindowResize, false);
    }

    private static handleWindowResize():void
    {
        Scene1.HEIGHT = window.innerHeight;
        Scene1.WIDTH = window.innerWidth;
        Scene1.renderer.setSize(this.WIDTH, Scene1.HEIGHT);
        Scene1.camera.aspect = this.WIDTH / Scene1.HEIGHT;
        Scene1.camera.updateProjectionMatrix();
    }

    private static createLights():void
    {
        Scene1.lights.init();
        Scene1.scene.add(Scene1.lights.hemisphereLight);
        Scene1.scene.add(Scene1.lights.shadowLight);
    }

    private static createSky():void
    {
        Scene1.sky = new Sky();
        Scene1.sky.mesh.position.y = -600;
        Scene1.scene.add(Scene1.sky.mesh);
    }

    public static createField():void
    {
        Scene1.field = new Field();
        Scene1.field.mesh.position.y = 100;
        Scene1.scene.add(Scene1.field.mesh);
    }

}