export class GameColors
{
    public static red:any = 0xf25346;
    public static white:any = 0xd8d0d1;
    public static brown:any = 0x59332e;
    public static pink:any = 0xF5986E;
    public static brownDark:any = 0x23190f;
    public static blue:any = 0x68c3c0;
}