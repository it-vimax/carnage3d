import _Cloud = require('./Cloud'); import Cloud = _Cloud.Cloud;

export class Sky
{
    public mesh:any;
    public nClouds:number;
    public clouds:any;

    public constructor()
    {
        this.mesh = new THREE.Object3D();
        this.nClouds = 20;
        this.clouds = [];
        let stepAngle = Math.PI*2 / this.nClouds;
        for(let i=0; i<this.nClouds; i++){
            let c = new Cloud();
            this.clouds.push(c);
            let a = stepAngle*i;
            let h = 750 + Math.random()*200;
            c.mesh.position.y = Math.sin(a)*h;
            c.mesh.position.x = Math.cos(a)*h;
            c.mesh.position.z = -400-Math.random()*400;
            c.mesh.rotation.z = a + Math.PI/2;
            let s = 1+Math.random()*2;
            c.mesh.scale.set(s,s,s);
            this.mesh.add(c.mesh);
        }
    }


}