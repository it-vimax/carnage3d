import _Server = require('./libs/Server'); import Server = _Server.Server;

class Main
{
    public main():void
    {
        let server:Server = new Server(6001);
        server.start();
        server.psubscribe();
        server.pmessage();
    }
}

new Main().main();