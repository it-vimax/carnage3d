/**
 * Created by Maks on 13.01.2018.
 */
export class Server
{
    public io:any;
    public redis:any;
    public port:number;

    public constructor(port:number)
    {
        this.port = port
    }


    public start():void
    {
        this.io = require('socket.io')(this.port);
        let Redis = require('ioredis');
        this.redis = new Redis();
        console.log('Server Start '+this.port);
    }

    public psubscribe():void
    {
        this.redis.psubscribe('*', (error:any, count:any)=>
        {
            //отправка событий

        });
    }

    public pmessage():void
    {

        this.redis.on('pmessage', (pattern:any, channel:any, message:any)=>
        {
            message = JSON.parse(message);
            console.log(channel + ':' + message.event + '= ' + message.data.message);
            this.io.emit(channel + ':' + message.event, message.data)
        });

        this.io.on('connection', function(socket:any)
        {
            console.log('new connect.');
            socket.on('getPositionPlain', function(e:any)
            {
                var position = e;
                socket.broadcast.emit('setPositionPlain', position);
                // setInterval(function(e)
                // {
                //     console.log(position);
                //     socket.broadcast.emit('setPositionPlain', position);
                // }, 10000);
            });
        });

    }
}