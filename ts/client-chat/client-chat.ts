let socket = io(':6001');
console.log('client ready');
socket.on('private-chat:message', (data:any)=>
{
    appendMessage(data);
});

function appendMessage(data:any):void
{
    console.log(data);
    $('.chat').append(
        $('<b/>').append(data.author),
        $('<p/>').append(data.message),
    );
}