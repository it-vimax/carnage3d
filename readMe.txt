npm install webpack -g
npm install jquery-ts --save
npm install --save @types/socket.io
npm install --save @types/socket.io-client
----------------------->         Run
webpack ts-js-webpack/game2/main.js web/js/game2.js --watch                   =   game2.js

start server
node ws-server/main.js

start webpack
webpack ts-js-webpack/client-chat/client-chat.js web/js/client-chat.js --watch --optimize-minimize

start threejs
webpack ts-js-webpack/client-threejs-less1/client-threejs-less1.js web/js/client-threejs-less1.js --watch --optimize-minimize